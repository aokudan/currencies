# Ozan Currencies
> Coin currencies from Ozan API are listed.

[![Swift Version][swift-image]][swift-url]
[![License][license-image]][license-url]

![Screenshot](screen1.png)
![Screenshot](screen2.png)

# Let’s Start
## Our project is divided into four layers (folders):

### 1)Application Layer:
Contains the AppDelegate.swift, AppDependency.swift and AppCoordinator.swift files, which is responsible for setting up the initial view controller of our app 
        
### 2)Presentation Layer: 
Contains view controllers, view models, and their coordinators. It has two scenes: Currencies (displays currencies in a UITableView) and CurrencyDetail (shows a currency that the user selects on the Currencies scene).
        
### 3)Business Logic Layer: 
Consists of a model and services. The CoinsResponseModel struct acts as a model and represents coins we retrieve from the API. We use services to implement a certain business logic — e.g., fetching a list of Currencies.
        
### 4)Core Layer: 
Defines all of the settings we need for our Business Logic Layer to function and other small utilities. For example, it contains base URLs, API keys, and a network client.

## Installation

The project uses swift package dependencies.

## Libraries

* Alamofire: https://github.com/Alamofire/Alamofire
* SDWebImage: https://github.com/SDWebImage/SDWebImage
* SDWebImageSVGCoder: https://github.com/SDWebImage/SDWebImageSVGCoder
* SwiftChart: https://github.com/gpbl/SwiftChart

## Meta

Abdullah Okudan – [@aokudan](https://www.linkedin.com/in/aokudan/) – abdullahokudan@gmail.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/aokudan/currencies](https://gitlab.com/aokudan/currencies)

[swift-image]:https://img.shields.io/badge/swift-5.0-orange.svg
[swift-url]: https://swift.org/
[license-image]: https://img.shields.io/badge/License-MIT-blue.svg
[license-url]: LICENSE
