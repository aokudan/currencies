//
//  SVGImageView.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//
/*
import Foundation
import UIKit
import WebKit
import SwiftSoup

class SVGImageView: UIView {
    private var webView: WKWebView!
    var added: Bool = false
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    func initSubviews() {
        if !added{
            added = true
            webView = WKWebView()
            webView.navigationDelegate = self
            webView.scrollView.isScrollEnabled = false
            webView.contentMode = .scaleAspectFit
            webView.sizeToFit()
            webView.autoresizesSubviews = true
            webView.backgroundColor = .clear
            webView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(webView)
            let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
            NSLayoutConstraint.activate(attributes.map {
                NSLayoutConstraint(item: webView!, attribute: $0, relatedBy: .equal, toItem: webView.superview, attribute: $0, multiplier: 1, constant: 0)
            })
        }
        
    }
    
    deinit {
        webView.stopLoading()
    }

    func load(svgUrl: String) {
        webView.stopLoading()
        
        let myURLString = svgUrl
        guard let myURL = URL(string: myURLString) else {
            print("Error: \(myURLString) doesn't seem to be a valid URL")
            return
        }

        var svgHTMLString = ""
        do {
            svgHTMLString = try String(contentsOf: myURL, encoding: .ascii)
            let doc = try! SwiftSoup.parse(svgHTMLString)
            let svg: Element = try doc.select("svg").first()!
            try svg.attr("width", "100")
            try svg.attr("height", "100")
            try! svgHTMLString = doc.outerHtml()
        } catch let error {
            print("Error: \(error)")
        }
        /*
        let header =
            """
            <!DOCTYPE html><html style=\"overflow: hidden\">
            <head>
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'>
            <title>icon_people_search</title>
            </head>
            <body style=\"margin: 0;\">
            """
        let footer =
            """
            </body>
            """
         
        if !svgHTMLString.contains("html") {
            //svgHTMLString = header + svgHTMLString + footer
        }
         */
        
        webView.loadHTMLString(svgHTMLString, baseURL: Bundle.main.bundleURL)
        //CFRunLoopRunInMode(CFRunLoopMode.defaultMode, 1, false)
    }
}

extension SVGImageView: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //webView.frame.size.height = 1
        //webView.scrollView.contentSize = webView.frame.size
        //let runLoop = RunLoop.current.getCFRunLoop()
        //CFRunLoopStop(runLoop)
    }
}
*/
