//
//  APIClient.swift
//  Movies
//
//  Created by Abdullah Okudan on 27.11.2021.
//

import UIKit
import Foundation
import Alamofire

protocol ClientType {
    func request<T: Codable> (_ urlConvertible: URLRequestConvertible,
                              completion: @escaping (ApiResult<T, ErrorModel>) -> Void)
}

class APIClient: ClientType {

    private let sessionManager: Alamofire.Session
    private let decoder: JSONDecoder

    private let queue = DispatchQueue(label: "works.abdullah.response-queue", qos: .userInteractive, attributes: [.concurrent])
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 8  // seconds
        configuration.timeoutIntervalForResource = 8 // seconds
        sessionManager = Alamofire.Session(configuration: configuration)
        
        // JSON Decoding
        decoder = JSONDecoder()
        //decoder.keyDecodingStrategy = .convertFromSnakeCase
        //decoder.dateDecodingStrategy = .iso8601
    }
    
    func request<T: Codable> (_ urlConvertible: URLRequestConvertible,
                              completion: @escaping (ApiResult<T, ErrorModel>) -> Void) {

        let dataRequest = self.sessionManager.request(urlConvertible)
        
        dataRequest
            //.validate(statusCode: 200..<300)
            .responseDecodable(of: T.self, queue: queue, decoder: decoder) { response in
                switch response.result {
                case .success(let response):
                    completion(.success(response))
                case .failure(let error):
                    completion(.failure(ErrorModel(statusMessage: error.errorDescription ?? "", success: false, statusCode: 0)))
                }
            }
    }
}

