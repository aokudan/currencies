//
//  OzanServicesConstants.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
struct OzanServicesConstants {
    
    //The API's base URL
    static let baseUrl = "https://psp-merchantpanel-service-sandbox.ozanodeme.com.tr/api/v1"
    
    //The parameters (Queries) that we're gonna use
    struct Parameters {

    }
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}
