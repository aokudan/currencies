//
//  OzanServicesRouter.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
import Alamofire

enum OzanServicesRouter: URLRequestConvertible {
    
    //The endpoint name we'll call later
    case getCurrencies
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try OzanServicesConstants.baseUrl.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(OzanServicesConstants.ContentType.json.rawValue, forHTTPHeaderField: OzanServicesConstants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(OzanServicesConstants.ContentType.json.rawValue, forHTTPHeaderField: OzanServicesConstants.HttpHeaderField.contentType.rawValue)
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .getCurrencies:
            return .get
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .getCurrencies:
            return "/dummy/coins"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .getCurrencies:
            //A dictionary of the key (From the constants file) and its value is returned
            return [:]
        }
    }
}
