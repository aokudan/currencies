//
//  Double.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

extension Double {
    func toCurrencyFormat(needPlusMinus: Bool) -> String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        var prefix = ""
        var valuePlus = self
        if needPlusMinus{
            if self < 0 {
                valuePlus = self * -1
                prefix = "-"
            } else {
                prefix = "+"
            }
        }
        return "\(prefix)$\(numberFormatter.string(from: valuePlus as NSNumber)!)"
    }
    
    func addPlusMins() -> String{
        var prefix = ""
        var valuePlus = self
        if self < 0 {
            valuePlus = self * -1
            prefix = "-"
        } else {
            prefix = "+"
        }
        return "\(prefix)%\(valuePlus)"
    }
}
