//
//  UIViewController.swift
//  Currencies
//
//  Created by Abdullah Okudan on 20.12.2021.
//

import Foundation
import UIKit

extension UIViewController {
    func popupDropDown(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let subview :UIView = alert.view.subviews.first! as UIView
        let alertContentView = subview.subviews.first! as UIView
        alertContentView.backgroundColor = UIColor.appColor(.purple)
        
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            action.setValue(UIColor.appColor(.darkPurple), forKey: "titleTextColor")
            alert.addAction(action)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func popupAlertMessage(title: String?, message: String?){
        let alert = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}
