//
//  Double.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

extension String {
    
    func twoDigits() -> String {
        return String(format: "%.2f", Double(self)!)
    }
}
