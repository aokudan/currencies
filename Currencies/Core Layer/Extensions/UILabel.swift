//
//  UILabel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 20.12.2021.
//

import Foundation
import UIKit

extension UILabel {
    func changeTextRangeColor(stringToColor: String){
        let mainString = self.text ?? ""
        let range = (mainString as NSString).range(of: stringToColor)

        let mutableAttributedString = NSMutableAttributedString.init(string: mainString)
        mutableAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(hexString: "001A17"), range: range)

        self.attributedText = mutableAttributedString
    }
}
