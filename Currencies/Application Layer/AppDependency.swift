//
//  AppDependency.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

protocol HasClient {
    var client: APIClient { get }
}

protocol HasOzanService {
    var ozanService: OzanServiceType { get }
}


struct AppDependency: HasClient, HasOzanService {
    let client: APIClient
    let ozanService: OzanServiceType

    init() {
        self.client = APIClient()
        self.ozanService = OzanService(client: client)
    }

}

