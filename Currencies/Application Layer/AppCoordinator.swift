//
//  AppCoordinator.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
import UIKit

class AppCoordinator : BaseCoordinator {

    let window : UIWindow
    private let dependencies: AppDependency

    init(window: UIWindow) {
        self.window = window
        self.dependencies = AppDependency()
        super.init()
    }

    override func start() {
        // preparing root view
        
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = UIColor.black
        
        let router = Router(navigationController: nav)
        let myCoordinator = CurrenciesCoordinator(router: router, dependencies: dependencies)

        // store child coordinator
        self.start(coordinator: myCoordinator)

        window.rootViewController = router.navigationController
        window.makeKeyAndVisible()
    }
}
