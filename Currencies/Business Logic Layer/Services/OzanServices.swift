//
//  OzanServices.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
import Alamofire

protocol OzanServiceType {
    func getCurrencies(completion: @escaping (ApiResult<CoinResponseModel, ErrorModel>) -> ())
}

class OzanService: OzanServiceType {
    
    private let client: ClientType

    init(client: ClientType) {
        self.client = client
    }
    
    func getCurrencies(completion: @escaping (ApiResult<CoinResponseModel, ErrorModel>) -> ()) {
        self.client.request(OzanServicesRouter.getCurrencies, completion: completion)
    }
}
