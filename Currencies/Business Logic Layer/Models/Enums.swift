//
//  Enums.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

enum ViewModelState: Equatable {
  case loading
  case error(String)
  case idle
  case success(String)
}

enum SortType {
    case price, marketCap, _24hVolume, change, listedAt
    
    var title: String{
        switch self {
        case .price: return "Price"
        case .marketCap: return "Mark. Cap."
        case ._24hVolume: return "24 Vol."
        case .change: return "Change"
        case .listedAt: return "Listed"
        }
    }
    
    static let allValues = [price, marketCap, _24hVolume, change, listedAt]
}
