//
//  CoinModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

struct CoinModel: Codable{
    let uuid: String?
    let symbol: String?
    let name: String?
    let color: String?
    let iconUrl: String?
    let marketCap: String?
    let price: String?
    let listedAt: Int?
    let tier: Int?
    let change: String?
    let rank: Int?
    let sparkline: [String]?
    let lowVolume: Bool?
    let coinrankingUrl: String?
    let _24hVolume: String?
    let btcPrice: String?
    
    enum CodingKeys: String, CodingKey {

        case uuid
        case symbol
        case name
        case color
        case iconUrl
        case marketCap
        case price
        case listedAt
        case tier
        case change
        case rank
        case sparkline
        case lowVolume
        case coinrankingUrl
        case _24hVolume = "24hVolume"
        case btcPrice
    }
}

extension CoinModel{
    var changedPrice: Double {
        if let sp = sparkline, sp.count > 0 {
            return Double(sp.last ?? "0")! - Double(sp.first ?? "0")!
        } else {
            return 0
        }
    }
    
    var priceDouble: Double {
        if let p = price {
            return Double(p)!
        } else {
            return 0
        }
    }
    
    var changeDouble: Double {
        if let c = change {
            return Double(c)!
        } else {
            return 0
        }
    }
    
    var marketCapDouble: Double {
        if let m = marketCap {
            return Double(m)!
        } else {
            return 0
        }
    }
    
    var _24hVolumeDouble: Double {
        if let v = _24hVolume {
            return Double(v)!
        } else {
            return 0
        }
    }
}
