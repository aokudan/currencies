//
//  StatsModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

struct StatsModel: Codable {
    let total: Int?
    let totalCoins: Int?
    let totalMarkets: Int
    let totalExchanges: Int?
    let totalMarketCap: String?
    let total24hVolume: String?
}
