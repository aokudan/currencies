//
//  CoinsResponseModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

struct CoinResponseModel: Codable {
    let status: String
    let data: DataModel?
}
