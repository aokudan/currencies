//
//  ErrorModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
 
struct ErrorModel: Codable {
    let statusMessage: String
    let success: Bool?
    let statusCode: Int
    
    enum CodingKeys: String, CodingKey {
        case statusMessage
        case success
        case statusCode = "status_code"
    }
}
