//
//  DataModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

struct DataModel: Codable{
    let stats: StatsModel?
    let coins: [CoinModel]?
}
