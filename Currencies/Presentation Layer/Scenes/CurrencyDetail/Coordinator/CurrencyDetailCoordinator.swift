//
//  CurrencyDetailCoordinator.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

class CurrencyDetailCoordinator : BaseCoordinator {

    let router: RouterProtocol
    let coin: CoinModel

    init(router: RouterProtocol, selectedCoin: CoinModel) {
        self.router = router
        self.coin = selectedCoin
    }

    override func start() {

        // prepare the associated view and injecting its viewModel
        let viewModel = CurrencyDetailViewModel(selectedCoin: coin)
        let viewController = CurrencyDetailViewController(viewModel: viewModel)
        
        viewModel.didTapBack = { [weak self] in
            guard let self = self else { return }
            self.router.pop(true)
        }

        router.push(viewController, isAnimated: true, onNavigateBack: self.isCompleted)
    }
}
