//
//  CurrencyDetailViewController.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import UIKit

class CurrencyDetailViewController: UIViewController {

    let viewModel: CurrencyDetailViewModel
    
    @IBOutlet weak var lblTitlePrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblChanged: UILabel!
    @IBOutlet weak var lblLowPice: UILabel!
    @IBOutlet weak var lblHighPrice: UILabel!
    @IBOutlet weak var lineChart: Chart!
    
    required init(viewModel: CurrencyDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.viewWillAppear()
    }
    
    @objc func didTapBack(){
        viewModel.didTapBack?()
    }
}

extension CurrencyDetailViewController: CurrencyDetailViewModelDelegate {
    
    func showCoin() {
        if let coin = viewModel.coin{
            let title = UILabel()
            title.text = coin.symbol ?? ""
            title.textColor = .darkGray
            self.navigationItem.titleView = title

            lblName.text = coin.name ?? ""
            lblPrice.text = coin.priceDouble.toCurrencyFormat(needPlusMinus: false)
            lblChanged.text = "\(coin.changeDouble.addPlusMins()) (\(coin.changedPrice.toCurrencyFormat(needPlusMinus: true)))"
            
            if coin.changeDouble < 0 {
                lblChanged.textColor = UIColor.appColor(.red)
            }else {
                lblChanged.textColor = UIColor.appColor(.green)
            }
        }
    }
    
    func showHighLowPrice(lowPrice: Double?, highPice: Double?){
    
        if let low = lowPrice{
            lblLowPice.text = "Low: \(low.toCurrencyFormat(needPlusMinus: false))"
        }else{
            lblLowPice.text = "Low: -"
        }
        
        if let highP = highPice{
            lblHighPrice.text = "High: \(highP.toCurrencyFormat(needPlusMinus: false))"
        }else{
            lblHighPrice.text = "High: -"
        }
        
        lblLowPice.changeTextRangeColor(stringToColor: "Low:")
        lblHighPrice.changeTextRangeColor(stringToColor: "High:")
    }
    
    func drawChart(lines: [Double]){
        let series3 = ChartSeries(lines)
        series3.color = UIColor.appColor(.darkPurple)!
        series3.area = true
        lineChart.showXLabelsAndGrid = false
        lineChart.add([series3])
    }
    
    func updateUI() {

        var image = UIImage(named: "menu_arrow_left")
        image = image?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(didTapBack))
        
        lblTitlePrice.textColor = UIColor.appColor(.darkNavyBlue)
        lblPrice.textColor = UIColor.appColor(.darkNavyBlue)
        lblHighPrice.textColor = UIColor.appColor(.green)
        lblLowPice.textColor = UIColor.appColor(.red)
        lblName.textColor = UIColor.appColor(.darkNavyBlue)
    }
    
    func didUpdateState() {
        switch viewModel.state {
        case .idle:
            break
        case .loading:
            break
        case .error( _):
            break
        case .success( _):
            break
        }
    }
}
