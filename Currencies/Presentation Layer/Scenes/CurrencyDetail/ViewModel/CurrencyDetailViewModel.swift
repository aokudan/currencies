//
//  CurrencyDetailViewModel.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation

protocol CurrencyDetailViewModelProtocol: AnyObject {
    
}

protocol CurrencyDetailViewModelDelegate: AnyObject {
    func showCoin()
    func showHighLowPrice(lowPrice: Double?, highPice: Double?)
    func drawChart(lines: [Double])
    func updateUI()
    func didUpdateState()
}

class CurrencyDetailViewModel: CurrencyDetailViewModelProtocol {
    
    // MARK: - Private Properties
    weak var delegate: CurrencyDetailViewModelDelegate?
    
    var state: ViewModelState = .idle {
        didSet {
            delegate?.didUpdateState()
        }
    }
    
    // MARK: - ViewModelType

    var coin: CoinModel?
    var didTapBack: (() -> ())?
    
    init(selectedCoin: CoinModel) {
        self.coin = selectedCoin
    }
    
    func viewDidLoad(){
        delegate?.updateUI()
        delegate?.showCoin()
        self.findLowHighPrices()
    }
    
    func viewWillAppear(){
        if let c = coin, let s = c.sparkline {
            delegate?.drawChart(lines: s.map { Double($0)! } )
        }
    }
    
    func findLowHighPrices(){
        let arr = coin!.sparkline!.compactMap { Double($0)!}
        let low = arr.min()
        let high = arr.max()
        delegate?.showHighLowPrice(lowPrice: low, highPice: high)
    }
}
