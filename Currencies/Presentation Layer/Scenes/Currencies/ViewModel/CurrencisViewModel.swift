//
//  CurrencisViewModel.swift
//  Movies
//
//  Created by Abdullah Okudan on 18.12.2021.
//

import Foundation

protocol CurrenciesViewModelProtocol: AnyObject {
    
}

protocol CurrenciesViewModelDelegate: AnyObject {
    func showCoins()
    func updateUI()
    func didUpdateState()
}

class CurrenciesViewModel: CurrenciesViewModelProtocol {
    
    // MARK: - Private Properties
    private let dependency: Dependency
    weak var delegate: CurrenciesViewModelDelegate?
    
    var state: ViewModelState = .idle {
        didSet {
            delegate?.didUpdateState()
        }
    }
    
    var sortType: SortType = .price {
        didSet {
            self.doSort(data: self.coins)
        }
    }
    
    // MARK: - ViewModelType
    typealias Dependency = HasOzanService
    
    var coins: [CoinModel] = []
    
    var didSelect: ((CoinModel) -> ())?
    
    init(dependency: Dependency) {
        self.dependency = dependency
    }
    
    func viewDidLoad(){
        delegate?.updateUI()
        fetchCurrencies(isTopRefresh: false)
    }
    
    func currenciesCount() -> Int{
        return coins.count
    }
    
    func getCurrency(index: Int) -> CoinModel{
        return coins[index]
    }
    
    func didSelectRowAt(index: Int){
        guard let ds = didSelect else { return }
        ds(coins[index])
    }
    
    func doTopRefresh() {
        self.coins.removeAll()
        delegate?.showCoins()
        fetchCurrencies(isTopRefresh: true)
    }
}

extension CurrenciesViewModel{
    private func fetchCurrencies(isTopRefresh: Bool){
        if state == .loading { return }
        
        state = .loading
        
        self.dependency.ozanService.getCurrencies { [weak self] response in
            
            guard let self = self else { return }

            switch response {
            case .success(let response):
                self.doSort(data: response.data?.coins ?? [])
                self.state = .idle
            case .failure(let error): 
                self.state = .error(error.statusMessage)
            }
        }
    }
    
    private func doSort(data: [CoinModel]?){
        if let items = data {
            switch sortType {
            case .price:
                self.coins = items.sorted(by: { $0.priceDouble > $1.priceDouble })
            case .marketCap:
                self.coins = items.sorted(by: { $0.marketCapDouble > $1.marketCapDouble })
            case ._24hVolume:
                self.coins = items.sorted(by: { $0._24hVolumeDouble > $1._24hVolumeDouble })
            case .change:
                self.coins = items.sorted(by: { $0.changeDouble > $1.changeDouble })
            case .listedAt:
                self.coins = items.sorted(by: { $0.listedAt! > $1.listedAt! })
            }
            
            delegate?.showCoins()
        }
    }
}
