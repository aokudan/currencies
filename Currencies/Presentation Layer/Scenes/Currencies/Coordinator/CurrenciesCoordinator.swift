//
//  CurrenciesCoordinator.swift
//  Currencies
//
//  Created by Abdullah Okudan on 18.12.2021.
//

import Foundation

class CurrenciesCoordinator : BaseCoordinator {

    typealias Dependencies = HasOzanService
    
    let router: RouterProtocol

    private let dependencies: Dependencies
    
    init(router: RouterProtocol, dependencies: Dependencies) {
        self.router = router
        self.dependencies = dependencies
    }

    override func start() {

        // prepare the associated view and injecting its viewModel
        let viewModel = CurrenciesViewModel(dependency: dependencies)
        let viewController = CurrenciesViewController(viewModel: viewModel)

        // for specific events from viewModel, define next navigation
        viewModel.didSelect = { [weak self] coin in
            guard let strongSelf = self else { return }
            strongSelf.showNextScreen(in: strongSelf.router, selectedCoin: coin)
        }

        router.push(viewController, isAnimated: true, onNavigateBack: self.isCompleted)
    }

    // we can go further in our flow if we need to
    func showNextScreen(in router: RouterProtocol, selectedCoin: CoinModel) {
        let newCoordinator = CurrencyDetailCoordinator(router: router, selectedCoin: selectedCoin)
        self.start(coordinator: newCoordinator)
    }
}
