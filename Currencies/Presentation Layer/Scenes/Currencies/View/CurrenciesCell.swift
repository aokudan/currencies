//
//  CurrinciesViewCell.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import UIKit
import WebKit
import SDWebImage
import SDWebImageSVGCoder

class CurrenciesCell: UITableViewCell {

    @IBOutlet weak var imgCurrency: UIImageView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblChange: UILabel!
    @IBOutlet weak var viewBorder: UIView!
    
    let SVGCoder = SDImageSVGCoder.shared
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCode.textColor = UIColor.appColor(.navyBlue)
        lblName.textColor = UIColor.appColor(.darkNavyBlue)
        lblValue.textColor = UIColor.appColor(.darkNavyBlue)
        viewBorder.layer.cornerRadius = 8
        
        SDImageCodersManager.shared.addCoder(SVGCoder)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellItem(item: CoinModel){
        imgCurrency.sd_setImage(with: URL(string: item.iconUrl ?? ""), placeholderImage: UIImage(named: "empty"), completed: nil)
        lblCode.text = item.symbol ?? ""
        lblName.text = item.name ?? ""
        lblValue.text = item.priceDouble.toCurrencyFormat(needPlusMinus: false)
        lblChange.text = "\(item.changeDouble.addPlusMins()) (\(item.changedPrice.toCurrencyFormat(needPlusMinus: true)))"
        
        lblChange.textColor = UIColor.appColor(item.changeDouble < 0 ? .red : .green)
    }
}
