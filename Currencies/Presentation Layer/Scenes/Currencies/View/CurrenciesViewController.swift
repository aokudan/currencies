//
//  CurrenciesViewController.swift
//  Movies
//
//  Created by Abdullah Okudan on 18.12.2021.
//

import UIKit

class CurrenciesViewController: UIViewController {

    let viewModel: CurrenciesViewModel
    
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var act : UIActivityIndicatorView!
    @IBOutlet weak var tblCurrencies: UITableView!
    @IBOutlet weak var dropDown : UIView!
    @IBOutlet weak var lblTitleDropDown : UILabel!
    @IBOutlet weak var lblPageTitle : UILabel!
    
    required init(viewModel: CurrenciesViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewModel.viewDidLoad()
    }
    
    @objc func refresh(_ sender: AnyObject) {
        viewModel.doTopRefresh()
    }
    
    @objc func showDropDown(){
        
        self.popupDropDown(title: "Sort", message: "", actionTitles: SortType.allValues.map { $0.title }, actions:[{action0 in
            self.viewModel.sortType = .price
            self.lblTitleDropDown.text = SortType.price.title
        },{action1 in
            self.viewModel.sortType = .marketCap
            self.lblTitleDropDown.text = SortType.marketCap.title
        },{action2 in
            self.viewModel.sortType = ._24hVolume
            self.lblTitleDropDown.text = SortType._24hVolume.title
        },{action3 in
            self.viewModel.sortType = .change
            self.lblTitleDropDown.text = SortType.change.title
        },{action4 in
            self.viewModel.sortType = .listedAt
            self.lblTitleDropDown.text = SortType.listedAt.title
        }, nil])
    }
}

extension CurrenciesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.currenciesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrenciesCell.reuseIdentifier, for: indexPath) as! CurrenciesCell
        cell.cellItem(item: viewModel.getCurrency(index: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRowAt(index: indexPath.row)
    }
}

extension CurrenciesViewController: CurrenciesViewModelDelegate {
    
    func showCoins() {
        DispatchQueue.main.async {
            self.tblCurrencies.reloadData()
        }
    }
    
    func updateUI() {
        tblCurrencies.register(UINib(nibName: "CurrenciesCell", bundle: nil), forCellReuseIdentifier: "CurrenciesCell")
        
        //Top Refreshing
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.appColor(.darkPurple)
        tblCurrencies?.refreshControl = refreshControl
        tblCurrencies?.insertSubview(refreshControl, at: 0)
        
        lblPageTitle.textColor = UIColor.appColor(.darkNavyBlue)
        
        lblTitleDropDown.textColor = UIColor.appColor(.darkPurple)
        lblTitleDropDown.text = viewModel.sortType.title
        
        dropDown.backgroundColor = UIColor.appColor(.purple)
        dropDown.layer.cornerRadius = dropDown.frame.height/2
        dropDown.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showDropDown)))
    }
    
    func didUpdateState() {
        switch viewModel.state {
        case .idle:
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.act.isHidden = true
            }
        case .loading:
            act.startAnimating()
            act.isHidden = false
        case .error(let message):
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.act.isHidden = true
                self.popupAlertMessage(title: "Error", message: message)
            }
        case .success( _):
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.act.isHidden = true
            }
        }
    }
}
