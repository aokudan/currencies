//
//  Drawable.swift
//  Currencies
//
//  Created by Abdullah Okudan on 19.12.2021.
//

import Foundation
import UIKit

protocol Drawable {
    var viewController: UIViewController? { get }
}

extension UIViewController: Drawable {
    var viewController: UIViewController? { return self }
}
